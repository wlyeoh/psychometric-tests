# Psychometric Tests

A python implementation of Attention Network Test (ANT), Remote Associates Test (RAT), an N-Back task, and an Anagram task using PyQt/PySide. Responses are recorded as csv files.

## Installation

```
pip install psychometric_tests
```

## Usage

- To start programme, run `psychometric_tests`.
    - **WARNING**: Ensure that the location of the installed scripts are in PATH.
- Adjust UI
    - **Ctrl+scroll** : Adjust font size
    - **F11** : Toggle full screen

### Attention Network Test (ANT)

The Attention Network Test (ANT) is designed to evaluate alerting, orienting,
and executive attention based on the difference in reaction time of pressing a
key indicating the direction of the central arrow. This implementation of ANT is
based on Fan *et al.* (2002).

- To start only ANT, run `psychometric_tests_ant`.

![Attention Network Test (ANT)](https://gitlab.com/wlyeoh/psychometric-tests/-/raw/master/images/ant.png)

### Remote Associates Test (RAT)

The Remote Associates Test (RAT) is sometimes used as measure of creative
potential by assessing a person's ability to think of a fourth word that is
somehow related to the three words presented. English questions from Bowden &
Jung-Beeman (2003) and Japanese questions from Terai, Miwa & Asami (2013) are
included.

- To start only RAT, run `psychometric_tests_rat`.

![Remote Associates Test (RAT)](https://gitlab.com/wlyeoh/psychometric-tests/-/raw/master/images/rat.png)

### N-Back

The N-Back task is commonly used to assess part of working memory.

- To start only N-Back, run `psychometric_tests_nback`.

![N-Back](https://gitlab.com/wlyeoh/psychometric-tests/-/raw/master/images/nback.png)


### Anagram Tasks

Anagram tasks are often used in cognitive research.

- To start only Anagram Tasks, run `psychometric_tests_anagram`.

![Anagram](https://gitlab.com/wlyeoh/psychometric-tests/-/raw/master/images/anagram.png)

## Customisation

The tests can be customised by editing the settings.json file (accessible from
the settings icon in main entry script).

The save name can include details provided in the test setup (e.g. Participant
ID, Date). To include the details, place the question text inside {}. For
example, "ID{Participant ID}-s{Session}-{Date-Time}.csv".

### Attention Network Test (ANT)

The arrows are drawn using unicode characters. The look and feel can be changed
by changing the font and the unicode character used.

The arrows and horizontal bar may not be aligned in some fonts.

### Remote Associates Test (RAT)

Different questions may be used. Change the "question_dir" setting from null to
your directory.

The question should be in the form of a csv file with a comma separating the
question and the solution. Each row is one question. The three questions should
be separated by forward slashes (/). Multiple solutions are possible; just
separate your solution with a forward slash.

For example, "high/district/house,school/court".

### N-Back

Similar to RAT, you can prepare your own set of stimulus. Change the "
stimulus_dir" setting from null to your directory.

The stimulus should be in the form of a csv file with a comma separating the
question and the solution.

For example, "6 + 2,8".

### Anagram Tasks

You can also prepare your own set of anagram questions. Change the "
question_dir" setting from null to your directory.

The question should be in the form of a csv file with commas separating each block and be in the correct order.

For example, "A,N,Y,T,H,I,N,G".

## Dependencies

- PySide6

## References

Bowden, E. M., & Jung-Beeman, M. (2003). Normative data for 144 compound remote
associate problems. Behavior research methods, instruments, & computers, 35(4),
634-639.

Fan, J., McCandliss, B.D., Sommer, T., Raz, A., et al. (2002) Testing the
Efficiency and Independence of Attentional Networks. Journal of Cognitive
Neuroscience. 14 (3), 340–347. Available from: doi:10.1162/089892902317361886.

寺井仁, 三輪和久, & 浅見和亮. (2013). 日本語版 Remote Associates Test の作成と評価. 心理学研究, 84(4),
419-428.
