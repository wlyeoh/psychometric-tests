import pathlib

from setuptools import setup, find_packages

here = pathlib.Path(__file__).parent.resolve()

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="psychometric_tests",
    version="0.1.1",
    author="WL Yeoh",
    author_email="wenliangyeoh@kyudai.jp",
    description="A python implementation of Attention Network Test (ANT), Remote Associates Test (RAT), an N-Back task, and an Anagram task.",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url="https://wlyeoh.gitlab.io/psychometric-tests/",
    project_urls={"Source": "https://gitlab.com/wlyeoh/psychometric-tests"},
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=['PySide6'],
    python_requires='>=3.6',
    entry_points={
        'gui_scripts': [
            'psychometric_tests=psychometric_tests.entry:run',
            'psych_tests=psychometric_tests.entry:run',
            'psychometric_tests_ant=psychometric_tests.entry:ant',
            'psychometric_tests_rat=psychometric_tests.entry:rat',
            'psychometric_tests_nback=psychometric_tests.entry:nback',
            'psychometric_tests_anagram=psychometric_tests.entry:anagram',
        ],
    },
)
